package de.leon.assembler.utils.objects;

import de.leon.assembler.utils.GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class RegisterList extends JTable {

    public RegisterList(String[][] rows, String[] columns) {

        super(rows, columns);
        this.setCellSelectionEnabled(false);

        this.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

                c.setBackground(row % 2 == 0 ? Color.decode(GUI.COLOR) : Color.WHITE);
                ((DefaultTableCellRenderer) c).setHorizontalAlignment(SwingConstants.CENTER);

                return c;
            }
        });
    }
}
