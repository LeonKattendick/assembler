package de.leon.assembler.utils.objects;

import de.leon.assembler.utils.enums.MnemonicType;

public class AssemblerRow {

    private MnemonicType type;
    private int value;

    public AssemblerRow(MnemonicType type, int value) {
        this.type = type;
        this.value = value;
    }

    public MnemonicType getType() {
        return type;
    }

    public int getValue() {
        return value;
    }
}
