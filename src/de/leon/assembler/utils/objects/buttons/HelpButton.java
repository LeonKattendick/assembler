package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;

import javax.swing.*;

public class HelpButton extends GUIButton {

    public HelpButton(GUI gui) {

        super("Hilfe", gui);

        String text = "<html>" +
                "Befehlssatz:<br><br>" +
                "&gt; DLOAD &lt;Zahl&gt; - Lädt die Zahl in den AC<br>" +
                "&gt; LOAD &lt;Register&gt; - Lädt den Inhalt des Registers in AC<br>" +
                "&gt; STORE &lt;Register&gt; - Speichert den Wert im AC in das Register<br>" +
                "<br>" +
                "&gt; ADD &lt;Register&gt; - Legt AC + Register in den AC<br>" +
                "&gt; SUB &lt;Register&gt; - Legt AC - Register in den AC<br>" +
                "&gt; DIV &lt;Register&gt; - Legt AC / Register in den AC (Ganze Zahl)<br>" +
                "&gt; MULT &lt;Register&gt; - Legt AC * Register in den AC<br>" +
                "<br>" +
                "&gt; JUMP &lt;Zeile&gt; - Setzt den PC auf diese Zeile<br>" +
                "&gt; JLE &lt;Zeile&gt; - Setzt den PC auf diese Zeile, wenn AC &lt;= 0<br>" +
                "&gt; JGE &lt;Zeile&gt; - Setzt den PC auf diese Zeile, wenn AC &gt;= 0<br>" +
                "&gt; JLT &lt;Zeile&gt; - Setzt den PC auf diese Zeile, wenn AC &lt; 0<br>" +
                "&gt; JGT &lt;Zeile&gt; - Setzt den PC auf diese Zeile, wenn AC &gt; 0<br>" +
                "<br>" +
                "&gt; END - Beendet das Programm<br>" +
                "<br><br><font size=2>Entwickelt von Leon Kattendick" +
                "<br>Mit Hilfe von Phillipp von Perponcher, 06.12.2018</font>" +
                "</html>";

        this.addActionListener(e -> JOptionPane.showMessageDialog(getGUI(), text, "Hilfe & Informationen", JOptionPane.PLAIN_MESSAGE));

    }
}
