package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.apis.CodeAPI;
import de.leon.assembler.utils.GUI;

public class SingleStepButton extends GUIButton {

    public SingleStepButton(GUI gui) {

        super("Nächster Schritt", gui);
        this.setEnabled(false);

        this.addActionListener(e -> {
            if (!getGUI().isEditing()) {
                CodeAPI.runNextStep(getGUI());
            }
        });
    }
}
