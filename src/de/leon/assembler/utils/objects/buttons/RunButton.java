package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;

public class RunButton extends GUIButton {

    public RunButton(GUI gui) {

        super("Starten", gui);

        this.addActionListener(e -> {
            if (getGUI().isEditing()) {
                getGUI().start();
            } else {
                getGUI().stop();
            }
        });
    }

    /**
     * Ändert den Names des Knopfes
     */
    public void change() {
        if (getGUI().isEditing()) {
            this.setText("Starten");
        } else {
            this.setText("Stoppen");
        }
    }
}
