package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;

import javax.swing.*;

public abstract class GUIButton extends JButton {

    private GUI gui;

    public GUIButton(String name, GUI gui) {
        super(name);
        this.gui = gui;
    }

    public GUI getGUI() {
        return gui;
    }
}
