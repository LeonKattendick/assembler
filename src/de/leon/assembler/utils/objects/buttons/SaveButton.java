package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SaveButton extends GUIButton {

    public SaveButton(GUI gui, JTextArea area) {

        super("Speichern", gui);

        this.addActionListener(e -> {

            JFileChooser chooser = new JFileChooser(new File("").getAbsolutePath());

            chooser.setDialogTitle("Speichern");
            chooser.setApproveButtonText("Speichern");
            chooser.setFileFilter(new FileNameExtensionFilter("Assembler Dateien (.asm)", "asm"));

            int ret = chooser.showOpenDialog(gui);

            if (ret == JFileChooser.APPROVE_OPTION) {
                try {

                    String end = ".asm";
                    if (chooser.getSelectedFile().getName().endsWith(".asm")) {
                        end = "";
                    }

                    FileWriter fw = new FileWriter(chooser.getSelectedFile() + end);

                    fw.write(area.getText());
                    fw.close();

                } catch (IOException ex) {
                }
            }
        });
    }
}
