package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;
import de.leon.assembler.utils.apis.CodeAPI;

import java.util.Timer;
import java.util.TimerTask;


public class FinishButton extends GUIButton {

    private Timer timer;

    public FinishButton(GUI gui) {

        super("Schneller Durchlauf", gui);
        this.setEnabled(false);

        this.timer = null;
        this.addActionListener(e -> start());

    }

    private void start() {
        if (this.timer == null) {
            this.timer = new Timer();
            this.timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (!getGUI().isEditing()) {
                        CodeAPI.runNextStep(getGUI());
                    } else {
                        stop();
                    }
                }
            }, 0, getGUI().getRunTime());
        }
    }

    public void stop() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }
}
