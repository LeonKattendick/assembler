package de.leon.assembler.utils.objects.buttons;

import de.leon.assembler.utils.GUI;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class OpenButton extends GUIButton {

    public OpenButton(GUI gui, JTextArea area) {

        super("Öffnen", gui);

        this.addActionListener(e -> {

            JFileChooser chooser = new JFileChooser(new File("").getAbsolutePath());

            chooser.setDialogTitle("Öffnen");
            chooser.setApproveButtonText("Öffnen");
            chooser.setFileFilter(new FileNameExtensionFilter("Assembler Dateien (.asm)", "asm"));

            int ret = chooser.showOpenDialog(gui);

            if (ret == JFileChooser.APPROVE_OPTION) {

                try {

                    String str, text = "";
                    BufferedReader br = new BufferedReader(new FileReader(chooser.getSelectedFile()));

                    while ((str = br.readLine()) != null) {
                        text = text + str + "\n";
                    }
                    area.setText(text.substring(0, text.length() - 1));

                    br.close();
                    gui.updateLines();

                } catch (IOException ex) {
                }
            }
        });
    }
}
