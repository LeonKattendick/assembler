package de.leon.assembler.utils.objects;

import de.leon.assembler.utils.GUI;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CodingArea extends JTextArea {

    public CodingArea(GUI gui) {

        super(45, 70);

        this.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                gui.updateLines();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                gui.updateLines();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                gui.updateLines();
            }
        });
    }
}
