package de.leon.assembler.utils.apis;

import de.leon.assembler.utils.GUI;

import java.io.*;

public class FilesAPI {

    /**
     * Initialisert die Dateien
     */
    public static void init() {

        File folder = new File("assembler/");
        File file1 = new File("assembler/temp.asm");
        File file2 = new File("assembler/settings.cfg");

        if (!folder.exists()) {
            folder.mkdir();
        }
        if (!file1.exists()) {
            try {
                file1.createNewFile();
            } catch (IOException e) {
            }
        }
        if (!file2.exists()) {
            try {

                file2.createNewFile();
                FileWriter writer = new FileWriter(file2);

                writer.write("autoSaveOnRun=true\nrunTimeInMilliseconds=200");
                writer.close();

            } catch (IOException e) {
            }
        }
    }

    /**
     * Lädt die Einstellungen
     *
     * @param gui > GUI für Variablen
     */
    public static void loadSettings(GUI gui) {

        File file = new File("assembler/settings.cfg");

        try {

            String str;
            int runTime = 200;
            boolean autoSave = true;
            BufferedReader br = new BufferedReader(new FileReader(file));

            while ((str = br.readLine()) != null) {
                if (str.contains("=")) {

                    String[] split = str.replaceAll(" ", "").split("=");

                    if (split.length == 2) {
                        try {
                            if (split[0].equals("autoSaveOnRun")) {
                                autoSave = Boolean.valueOf(split[1]);
                            } else {
                                runTime = Integer.valueOf(split[1]);
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            }
            if (runTime < 25) {
                runTime = 200;
            }

            gui.setRunTime(runTime);
            gui.setAutoSave(autoSave);

        } catch (IOException e) {
        }
    }

    /**
     * Speichert den Code automatisch in "assembler/temp.asm"
     *
     * @param text > Text der gespeichert werden soll
     */
    public static void autoSaveCode(String text) {

        File file = new File("assembler/temp.asm");

        if (file.exists()) {
            try {

                FileWriter writer = new FileWriter(file);

                writer.write(text);
                writer.close();

            } catch (IOException e) {
            }
        }
    }
}
