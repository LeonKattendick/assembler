package de.leon.assembler.utils.apis;

import de.leon.assembler.utils.GUI;
import de.leon.assembler.utils.enums.ErrorType;
import de.leon.assembler.utils.enums.MnemonicType;
import de.leon.assembler.utils.objects.AssemblerRow;

import javax.swing.*;

public class CodeAPI {

    private static String[] TEXT_CODE;
    private static AssemblerRow[] CODE;
    private static String WRONG_LINE;
    private static int WRONG_LINE_NUMBER;

    /**
     * Initialisiert den Code für die Überprüfung + Ausführung
     *
     * @param area > Das Textfeld mit dem Code
     */
    public static void init(JTextArea area) {

        TEXT_CODE = new String[area.getLineCount()];

        int i = 0;
        for (String entry : area.getText().split("\n")) {
            TEXT_CODE[i++] = entry.replaceAll("\t", "   ");
        }
    }

    /**
     * Überprüft die Richtigkeit des Codes
     *
     * @return ErrorType der ersten falschen Zeile
     */
    public static ErrorType isReadable() {

        int line = 1;
        boolean hasEnd = false;
        for (String entry : TEXT_CODE) {

            WRONG_LINE = entry;
            WRONG_LINE_NUMBER = line++;
            if (!isOnlySpaceLine(entry)) {
                if (entry != null && !(entry + " ").toUpperCase().startsWith("END ") && entry.contains(" ") && entry.split(" ").length >= 2) {

                    String[] cmd = entry.split(" ");

                    if (cmd.length > 2) {

                        boolean comment = false;
                        for (int i = 2; i < cmd.length; i++) {
                            if (!cmd[i].equals("") && (cmd[i].startsWith("--") || cmd[i].startsWith(";")|| cmd[i].startsWith("//"))) {
                                comment = true;
                            } else if (!cmd[i].equals("")) {
                                break;
                            }
                        }
                        if (!comment) {
                            return ErrorType.WRONG_COMMENT;
                        }
                    }
                    try {
                        MnemonicType.valueOf(cmd[0].toUpperCase());
                    } catch (IllegalArgumentException e) {
                        return ErrorType.WRONG_COMMAND;
                    }
                    try {
                        Integer.valueOf(cmd[1]);
                    } catch (NumberFormatException e) {
                        return ErrorType.WRONG_VALUE;
                    }
                } else if (entry != null && !(entry + " ").toUpperCase().startsWith("END ") && !entry.equalsIgnoreCase("")) {
                    return ErrorType.WRONG_COMMAND;
                } else if (entry != null && (entry + " ").toUpperCase().startsWith("END ")) {
                    hasEnd = true;
                }
            }
        }
        if (!hasEnd) {
            return ErrorType.NO_END;
        }
        return ErrorType.ALLOWED;
    }

    /**
     * Interne Sortierung des Codes
     */
    public static void parseCode() {

        CODE = new AssemblerRow[TEXT_CODE.length];

        for (int i = 0; i < TEXT_CODE.length; i++) {
            if (!isOnlySpaceLine(TEXT_CODE[i])) {
                if (TEXT_CODE[i] != null && !TEXT_CODE[i].equalsIgnoreCase("")) {
                    if (!(TEXT_CODE[i] + " ").toUpperCase().startsWith("END ")) {
                        CODE[i] = new AssemblerRow(MnemonicType.valueOf(TEXT_CODE[i].split(" ")[0].toUpperCase()), Integer.valueOf(TEXT_CODE[i].split(" ")[1]));
                    } else {
                        CODE[i] = new AssemblerRow(MnemonicType.END, -1);
                    }
                } else {
                    CODE[i] = new AssemblerRow(MnemonicType.EMPTY, -1);
                }
            } else {
                CODE[i] = new AssemblerRow(MnemonicType.EMPTY, -1);
            }
        }
    }

    /**
     * Führt den nächsten Schritt im Code durch
     *
     * @param gui > GUI für Frame Updates
     */
    public static void runNextStep(GUI gui) {
        try {

            AssemblerRow row = CODE[gui.getProgramCounter() - 1];

            if (row.getType() != MnemonicType.END) {
                if (row.getType() == MnemonicType.DLOAD) {
                    gui.setAccumulator(row.getValue());
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.LOAD) {
                    gui.setAccumulator(gui.getRegister().get(row.getValue() - 1));
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.STORE) {
                    gui.getRegister().put(row.getValue() - 1, gui.getAccumulator());
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.ADD) {
                    gui.setAccumulator(gui.getAccumulator() + gui.getRegister().get(row.getValue() - 1));
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.SUB) {
                    gui.setAccumulator(gui.getAccumulator() - gui.getRegister().get(row.getValue() - 1));
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.DIV) {
                    gui.setAccumulator(gui.getAccumulator() / gui.getRegister().get(row.getValue() - 1));
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.MULT) {
                    gui.setAccumulator(gui.getAccumulator() * gui.getRegister().get(row.getValue() - 1));
                    gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                } else if (row.getType() == MnemonicType.JUMP) {
                    gui.setProgramCounter(row.getValue() + getNextValidLineJump(row.getValue()));
                } else if (row.getType() == MnemonicType.JLE) {
                    if (gui.getAccumulator() <= 0) {
                        gui.setProgramCounter(row.getValue() + getNextValidLineJump(row.getValue()));
                    } else {
                        gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                    }
                } else if (row.getType() == MnemonicType.JGE) {
                    if (gui.getAccumulator() >= 0) {
                        gui.setProgramCounter(row.getValue() + getNextValidLineJump(row.getValue()));
                    } else {
                        gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                    }
                } else if (row.getType() == MnemonicType.JLT) {
                    if (gui.getAccumulator() < 0) {
                        gui.setProgramCounter(row.getValue() + getNextValidLineJump(row.getValue()));
                    } else {
                        gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                    }
                } else if (row.getType() == MnemonicType.JGT) {
                    if (gui.getAccumulator() > 0) {
                        gui.setProgramCounter(row.getValue() + getNextValidLineJump(row.getValue()));
                    } else {
                        gui.setProgramCounter(gui.getProgramCounter() + getNextValidLine(gui));
                    }
                }

                gui.updateList(false);
                gui.updateCounters(false);
                gui.updateLines();
                gui.update();

            } else {
                gui.stop();
            }
        } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
            exitError(gui, ErrorType.WRONG_JUMP_OR_REG);
        }
    }

    /**
     * Beendet den Ausfürungsmodus mit einer Fehlermeldung
     *
     * @param gui  > Frame für JOptionPane
     * @param type > ErrorType des aufgetretenen Fehlers
     */
    public static void exitError(GUI gui, ErrorType type) {
        gui.stop();
        ErrorType.throwError(type, "Es trat ein Fehler auf!");
    }

    /**
     * Gibt den Inhalt der letzten falschen Zeile zurück
     *
     * @return String > Inhalt der falschen Zeile
     */
    public static String getWrongLine() {
        return WRONG_LINE;
    }

    /**
     * Gibt die Zeilennummer der letzten falschen Zeile zurück
     *
     * @return int > Zeilennummer
     */
    public static int getWrongLineNumber() {
        return WRONG_LINE_NUMBER;
    }

    /**
     * Gibt die erste Zeile mit Code zurück
     *
     * @param gui > GUI für PC
     * @return int > Erste Zeile
     */
    public static int getFirstValidLine(GUI gui) {
        return getFirstLine(0, gui);
    }


    /**
     * Gibt die nächste Zeile ausgehend vom PC die Code enthält
     *
     * @param gui > GUI für PC
     * @return int > Nächste Zeile (Muss noch auf PC addiert werden)
     */
    public static int getNextValidLine(GUI gui) {
        return getFirstLine(1, gui);
    }

    /**
     * Gibt die erste richtige Zeile ausgehend von PC + Count zurück
     *
     * @param count > Versetzung des PC
     * @param gui   > GUI für PC
     * @return int > Erste Zeile mit Code
     */
    private static int getFirstLine(int count, GUI gui) {
        while (CODE.length > gui.getProgramCounter() + count && CODE[gui.getProgramCounter() - 1 + count].getType() == MnemonicType.EMPTY) {
            count++;
        }
        return count;
    }

    /**
     * Gibt die nächste Zeile nach einem Jump zuürck die Code enthält
     *
     * @param row > Zeile in die gejumpt wird
     * @return int > Nächste Zeile
     */
    public static int getNextValidLineJump(int row) {

        int count = 0;
        while (CODE.length > row + count && CODE[row - 1 + count].getType() == MnemonicType.EMPTY) {
            count++;
        }
        return count;
    }

    /**
     * Gibt zurück ob ein String nur aus Leertasten besteht
     *
     * @param line > Zeile die zu überprüfen ist
     * @return boolean > Ob die Zeile nur aus Leertasten besteht
     */
    private static boolean isOnlySpaceLine(String line) {

        boolean ret = false;

        if (line != null) {

            ret = true;
            for (char c : line.toCharArray()) {
                if (c != ' ') {
                    ret = false;
                }
            }
        }
        return ret;
    }
}
