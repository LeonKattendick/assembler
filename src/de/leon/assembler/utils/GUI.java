package de.leon.assembler.utils;

import de.leon.assembler.utils.apis.CodeAPI;
import de.leon.assembler.utils.apis.FilesAPI;
import de.leon.assembler.utils.enums.ErrorType;
import de.leon.assembler.utils.objects.*;
import de.leon.assembler.utils.objects.buttons.*;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class GUI extends JFrame {

    public static String COLOR = "#DFDFF0";

    private static int REGISTER_AMOUNT = 40;
    private static int DISTANCE_NORTH = 5, DISTANCE_WEST = 5, DISTANCE_SOUTH = -12;
    private static int DISTANCE_NORTH_REGISTER = 97, DISTANCE_NORTH_AC = 45, DISTANCE_BUTTON_SPACE = 5, DISTANCE_BUTTON_BIGGER_SPACE = 40, DISTANCE_AREA_SPACE = 5, DISTANCE_AC_SPACE = 20;

    private JScrollPane areaScroll, listScroll;
    private JTextArea area, lines;
    private JLabel accumulatorLabel, programLabel;
    private SpringLayout layout;
    private JButton open, save, run, step, finish, help;

    private boolean editing, autoSave;
    private int accumulator, programCounter, runTime;
    private HashMap<Integer, Integer> register;

    public GUI() {

        super("Assembler");

        this.editing = true;
        this.accumulator = 0;
        this.programCounter = 1;
        this.register = new HashMap<>();

        resetRegister();

        // ------------ FRAME SETTINGS ------------

        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(1300, 800);
        this.setLocationRelativeTo(null);

        // ------------ LAYOUT SETTINGS ------------

        this.layout = new SpringLayout();
        this.getContentPane().setLayout(this.layout);

        // ------------ TEXT AREA CREATION + LINES ------------

        this.area = new CodingArea(this);
        this.areaScroll = new JScrollPane(this.area);
        this.areaScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        this.lines = new JTextArea();

        this.lines.setText(" 01 \n");
        this.lines.setBackground(Color.decode(COLOR));
        this.lines.setEditable(false);

        this.areaScroll.getViewport().add(this.area);
        this.areaScroll.setRowHeaderView(this.lines);

        // ------------ BUTTON CREATION ------------

        this.open = new OpenButton(this, this.area);
        this.save = new SaveButton(this, this.area);

        this.run = new RunButton(this);
        this.step = new SingleStepButton(this);
        this.finish = new FinishButton(this);

        this.help = new HelpButton(this);

        // ------------ REGISTER CREATION  ------------

        updateList(true);
        updateCounters(true);

        // ------------ FILL CONTENTPANE ------------

        this.getContentPane().add(this.open);
        this.getContentPane().add(this.save);
        this.getContentPane().add(this.run);
        this.getContentPane().add(this.step);
        this.getContentPane().add(this.finish);
        this.getContentPane().add(this.help);
        this.getContentPane().add(this.areaScroll);

        // ------------ LAYOUT POSITIONS #2 ------------

        this.layout.putConstraint(SpringLayout.NORTH, this.open, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.open, DISTANCE_WEST, SpringLayout.WEST, this.getContentPane());

        this.layout.putConstraint(SpringLayout.NORTH, this.save, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.save, DISTANCE_BUTTON_SPACE, SpringLayout.EAST, this.open);

        this.layout.putConstraint(SpringLayout.NORTH, this.run, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.run, DISTANCE_BUTTON_BIGGER_SPACE, SpringLayout.EAST, this.save);

        this.layout.putConstraint(SpringLayout.NORTH, this.step, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.step, DISTANCE_BUTTON_SPACE, SpringLayout.EAST, this.run);

        this.layout.putConstraint(SpringLayout.NORTH, this.finish, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.finish, DISTANCE_BUTTON_SPACE, SpringLayout.EAST, this.step);

        this.layout.putConstraint(SpringLayout.NORTH, this.help, DISTANCE_NORTH, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.help, DISTANCE_BUTTON_BIGGER_SPACE, SpringLayout.EAST, this.finish);

        this.layout.putConstraint(SpringLayout.NORTH, this.areaScroll, DISTANCE_BUTTON_SPACE, SpringLayout.SOUTH, this.open);
        this.layout.putConstraint(SpringLayout.WEST, this.areaScroll, DISTANCE_WEST, SpringLayout.WEST, this.getContentPane());

        // ------------ FINISH ------------

        FilesAPI.init();
        FilesAPI.loadSettings(this);
        this.setVisible(true);

    }

    public int getAccumulator() {
        return accumulator;
    }

    public int getProgramCounter() {
        return programCounter;
    }

    public int getRunTime() {
        return runTime;
    }

    public HashMap<Integer, Integer> getRegister() {
        return register;
    }

    public void setAccumulator(int accumulator) {
        this.accumulator = accumulator;
    }

    public void setProgramCounter(int programCounter) {
        this.programCounter = programCounter;
    }

    public void setRunTime(int runTime) {
        this.runTime = runTime;
    }

    public boolean isEditing() {
        return editing;
    }

    public boolean isAutoSave() {
        return autoSave;
    }

    public void setAutoSave(boolean autoSave) {
        this.autoSave = autoSave;
    }

    /**
     * Revalidiert alles Items auf der ContentPane
     */
    public void update() {
        this.revalidate();
        this.repaint();
    }

    /**
     * Updated die Zeilennummerierung
     */
    public void updateLines() {
        this.lines.setText(getText(this.area));
    }

    /**
     * Updated die Register
     *
     * @param startup > Wurde die Funktion im Konstruktor ausgeführt
     */
    public void updateList(boolean startup) {

        String[][] rows = new String[REGISTER_AMOUNT][REGISTER_AMOUNT];

        for (int i = 0; i < rows.length; i++) {
            rows[i][0] = "R" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1));
            rows[i][1] = this.register.get(i) + "";
        }
        if (!startup) {
            this.getContentPane().remove(this.listScroll);
        }

        JScrollPane scroll = new JScrollPane(new RegisterList(rows, new String[]{"Register", "Werte"}));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        this.listScroll = scroll;
        this.getContentPane().add(this.listScroll);

        this.layout.putConstraint(SpringLayout.NORTH, this.listScroll, DISTANCE_NORTH_REGISTER, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.listScroll, DISTANCE_AREA_SPACE, SpringLayout.EAST, this.areaScroll);
        this.layout.putConstraint(SpringLayout.SOUTH, this.listScroll, DISTANCE_SOUTH, SpringLayout.SOUTH, this.getContentPane());

    }

    /**
     * Updated den AC und PC
     *
     * @param startup > Wurde die Funktion im Konstruktor ausgeführt
     */
    public void updateCounters(boolean startup) {
        if (!startup) {
            this.getContentPane().remove(this.programLabel);
            this.getContentPane().remove(this.accumulatorLabel);
        }

        this.accumulatorLabel = new JLabel("Akkumulator (AC): " + getAccumulator());
        this.accumulatorLabel.setFont(this.accumulatorLabel.getFont().deriveFont(13f));

        this.programLabel = new JLabel("Befehlszähler (PC): " + getProgramCounter());
        this.programLabel.setFont(this.programLabel.getFont().deriveFont(13f));

        this.getContentPane().add(this.programLabel);
        this.getContentPane().add(this.accumulatorLabel);

        this.layout.putConstraint(SpringLayout.NORTH, this.accumulatorLabel, DISTANCE_NORTH_AC, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.accumulatorLabel, DISTANCE_AREA_SPACE, SpringLayout.EAST, this.areaScroll);

        this.layout.putConstraint(SpringLayout.NORTH, this.programLabel, DISTANCE_AC_SPACE, SpringLayout.NORTH, this.accumulatorLabel);
        this.layout.putConstraint(SpringLayout.WEST, this.programLabel, DISTANCE_AREA_SPACE, SpringLayout.EAST, this.areaScroll);

    }

    /**
     * Resetet Counter und Register
     */
    public void reset() {

        resetRegister();

        setAccumulator(0);
        setProgramCounter(1);

        updateCounters(false);
        updateList(false);
        update();

    }

    /**
     * Resetet die Register
     */
    private void resetRegister() {
        for (int i = 0; i < REGISTER_AMOUNT; i++) {
            this.register.put(i, 0);
        }
    }

    /**
     * Startet den Ausführungsmodus
     */
    public void start() {

        ErrorType type;
        CodeAPI.init(this.area);

        if ((type = CodeAPI.isReadable()) == ErrorType.ALLOWED) {

            this.editing = false;
            this.area.setEditable(false);
            ((RunButton) this.run).change();

            this.open.setEnabled(false);
            this.save.setEnabled(false);

            this.step.setEnabled(true);
            this.finish.setEnabled(true);

            reset();
            updateLines();
            CodeAPI.parseCode();

            setProgramCounter(1 + CodeAPI.getFirstValidLine(this));
            updateLines();
            updateCounters(false);

            if (isAutoSave()) {
                FilesAPI.autoSaveCode(this.area.getText());
            }
        } else {
            if (type == ErrorType.NO_END) {
                ErrorType.throwError(type, "Das Programm hat kein Ende!");
            } else {
                ErrorType.throwError(type, "Fehler in Zeile " + CodeAPI.getWrongLineNumber() + ": '" + CodeAPI.getWrongLine() + "'");
            }
        }
    }

    /**
     * Beendet den Ausführungsmodus
     */
    public void stop() {

        this.editing = true;
        this.area.setEditable(true);
        ((RunButton) this.run).change();

        this.open.setEnabled(true);
        this.save.setEnabled(true);

        this.step.setEnabled(false);
        this.finish.setEnabled(false);

        updateLines();
        update();

    }

    /**
     * Gibt den Text für die Zeilennummern zurück
     *
     * @param area > Text mit Code
     * @return String > Inhalt für das Lines-Feld
     */
    private String getText(JTextArea area) {

        int pos = area.getLineCount() - 1;
        String text = " 01 \n";

        if (!isEditing() && this.programCounter == 1) {
            text = "\u2B9E 01 \n";
        }
        for (int i = 2; i < pos + 2; i++) {
            if (!isEditing() && this.programCounter == i) {
                text += "\u2B9E " + (i < 10 ? "0" + i : i) + " \n";
            } else {
                text += " " + (i < 10 ? "0" + i : i) + " \n";
            }
        }
        return text;
    }
}
