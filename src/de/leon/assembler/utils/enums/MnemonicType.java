package de.leon.assembler.utils.enums;

public enum MnemonicType {
    DLOAD, LOAD, STORE, ADD, SUB, DIV, MULT, JUMP, JLE, JGE, JLT, JGT, END, EMPTY;
}
