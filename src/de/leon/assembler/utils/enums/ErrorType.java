package de.leon.assembler.utils.enums;

import javax.swing.*;

public enum ErrorType {

    ALLOWED(0, "Erlaubt"), WRONG_COMMENT(1, "Falsche Kommentierung"), WRONG_COMMAND(2, "Falscher Befehl"), WRONG_VALUE(3, "Falsche Eingabe"), NO_END(4, "Kein Ende"), WRONG_JUMP_OR_REG(5, "Ungültige(s) Adresse / Register");

    private int code;
    private String name;

    private ErrorType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    /**
     * Wirft ein JOptionPane mit der Fehlermeldung + ErrorType als Überschrift
     *
     * @param type  > Letzter ErrorType
     * @param input > Auszugebende Nachricht
     */
    public static void throwError(ErrorType type, String input) {
        JOptionPane.showMessageDialog(null, input, type.getName(), JOptionPane.ERROR_MESSAGE);
    }
}
